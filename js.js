const nick = prompt('Digite Seu Nick');
alert(`O jogo é simples ${nick} basta escolher sua opção e clicar em jogar para ter o resultado na tela!`)
document.getElementById('nick').innerHTML = nick;
const visit = document.getElementById('div2');

// jogada do computador
// const pedraPc = document.getElementById('pedra');
// const papelPc = document.getElementById('papel');
// const tesouraPc = document.getElementById('tesoura');
// ----------Escolher numero aleatorio para o pc
let numAleatorio;
function numPC(min,max){
    numAleatorio = Math.floor(Math.random() * (max - min) ) + min;    
}



// ações do visitante

const jogar = document.getElementById('play')
jogar.addEventListener('click', fn)

let meuNum;

let stone = document.getElementById('pedra1');
stone.addEventListener('click', function(){
    meuNum=1;
    document.getElementById('play').classList.remove('hidden')
})
let paper = document.getElementById('papel1');
paper.addEventListener('click', function(){
    meuNum=2;
    document.getElementById('play').classList.remove('hidden')
})
let scissors = document.getElementById('tesoura1');
scissors.addEventListener('click', function(){
    meuNum=3;
    document.getElementById('play').classList.remove('hidden')
})
function fn() {
    numPC(1,4);
    if(numAleatorio == meuNum){
        visit.classList="tie"
        visit.innerHTML = `${nick} você Empatou com a maquina!`
    }

    if(numAleatorio == 1 && meuNum == 2){
        visit.classList='winner'
        visit.innerHTML= `${nick} você ganhou`
    }

    if(numAleatorio == 1 && meuNum == 3){
        visit.classList='loser'
        visit.innerHTML= `${nick} você Perdeu!`
    }

    if(numAleatorio == 2 && meuNum == 1){
        visit.classList='loser'
        visit.innerHTML= `${nick} você Perdeu!`
    }

    if(numAleatorio == 2 && meuNum == 3){
        visit.classList='winner'
        visit.innerHTML= `${nick} você Ganhou!`
    }

    if(numAleatorio == 3 && meuNum == 1){
        visit.classList='winner'
        visit.innerHTML= `${nick} você Ganhou!`
    }

    if(numAleatorio == 3 && meuNum == 2){
        visit.classList='loser'
        visit.innerHTML= `${nick} você Perdeu!`
    }
}